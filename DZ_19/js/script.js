"use strict";
let storyPoint = [3, 3, 4];
let beckLog = [10, 7, 12, 3, 23];
let deadLine = new Date(2021, 2, 11);

function deadLineCounter(spForADay, bLog, endDate) {
  let commandSrotyPoint = spForADay.reduce((sum, current) => sum + current, 0);
  let allBeckLogs = bLog.reduce((sum, current) => sum + current, 0);
  let daysToFinish = allBeckLogs / commandSrotyPoint;

  let startDate = new Date();
  let millisecondsOfDay = 86400 * 1000;
  startDate.setHours(0, 0, 0, 1);
  endDate.setHours(23, 59, 59, 999);
  let diff = endDate - startDate;
  let daysToDeadLine = Math.ceil(diff / millisecondsOfDay);
  let weeks = Math.floor(daysToDeadLine / 7);
  daysToDeadLine = daysToDeadLine - weeks * 2;

  if (daysToDeadLine >= daysToFinish) {
    let elseDay = Math.floor(daysToDeadLine - daysToFinish);
    alert(
      `Все задачи будут успешно выполнены за ${elseDay} дней до наступления дедлайна!`
    );
  } else {
    let hoursToFinish = Math.trunc((daysToFinish - daysToDeadLine) * 8);
    alert(
      `Команде разработчиков придется потратить дополнительно ${hoursToFinish} часов после дедлайна, чтобы выполнить все задачи в беклоге`
    );
  }
}
deadLineCounter(storyPoint, beckLog, deadLine);
