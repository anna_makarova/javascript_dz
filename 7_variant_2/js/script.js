"use strict";
let array = [
  "hello",
  "world",
  "Kiev",
  ["Borispol", "Irpin"],
  "Kharkiv",
  "Odessa",
  "Lviv",
];
let parent = document.body;

function displayAList(array, parent) {
  const ul = document.createElement("ul");
  array.forEach((iterator) => {
    if (Array.isArray(iterator)) {
      displayAList(iterator, ul);
    } else {
      ul.innerHTML += `<li>${iterator}</li>`;
    }
    parent.append(ul);
  });
}
displayAList(array, parent);
function timing(sec) {
  const span = document.createElement("span");
  const deletEl = document.querySelector("ul");
  span.textContent = sec;
  document.body.append(span);
  setInterval(() => {
    span.innerHTML = --sec;
    if (sec === -1) {
      deletEl.remove();
      span.remove();
    }
  }, 1000);
}
timing(3);
