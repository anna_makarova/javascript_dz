"use strict";

const btns = document.querySelectorAll(".btn");

window.addEventListener("keydown", keyboardPressHandler);
function keyboardPressHandler(event) {
  btns.forEach((btn) => {
    btn.classList.remove("blue-btn");
    if (event.code === btn.dataset.key) {
      btn.classList.add("blue-btn");
    }
  });
}
