"use strict";
function createNewUser() {
  const newUser = {
    firstName: "",
    lastName: "",
    getLogin() {
      return (
        this.firstName.substring(0, 1).toLowerCase() +
        this.lastName.toLowerCase()
      );
    },
  };
  do {
    newUser.firstName = prompt("Your name?");
  } while (
    !newUser.firstName ||
    newUser.firstName.trim === "" ||
    !isNaN(+newUser.firstName)
  );
  do {
    newUser.lastName = prompt("Your last name?");
  } while (
    !newUser.lastName ||
    newUser.lastName.trim === "" ||
    !isNaN(+newUser.lastName)
  );

  return newUser;
}
const user = createNewUser();
console.log(user.getLogin());
