"use strict";

$(function () {
  let margin = 10;
  $(".navigation-link").click(function () {
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top + margin + "px",
      },
      {
        duration: 1600,
        easing: "swing",
      }
    );
  });
  $(document).scroll(function () {
    if (window.pageYOffset > 700) {
      $(".btn-for-skroll").show("slow");
    }
  });
  $(".btn-for-skroll").click(function () {
    $("html, body").animate({ scrollTop: "0px" }, 1000);
  });

  $(".btn-for-hide").click(function () {
    $(".top-rated").slideToggle(2000);
  });
});
