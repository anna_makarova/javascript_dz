"use strict";
const student = {
  name: null,
  lastName: null,
  tabel: {},
};
let subject;
let assessment;

function numberValidation(a) {
  return !a || a.trim() === "" || isNaN(+a);
}
function stringValidation(b) {
  return !b || b.trim() === "" || !isNaN(+b);
}
do {
  student.name = prompt("What is your name?");
} while (stringValidation(student.name));
do {
  student.lastName = prompt("What is your last name?");
} while (stringValidation(student.lastName));

do {
  subject = prompt("What is the subject?");
} while (stringValidation(subject));
do {
  assessment = prompt("What is your assessment for this subject?");
} while (numberValidation(assessment));

while (subject) {
  student.tabel[subject] = assessment;
  subject = prompt("What is the subject?");
  if (!subject) {
    break;
  }
  assessment = prompt("What is your assessment for this subject?");
}
let result = 0;
let sum = 0;
let counterRange = 0;
let badAssessment = false;

for (let key in student.tabel) {
  if (student.tabel[key] < 4) {
    badAssessment = true;
    break;
  }
}
if (!badAssessment) {
  console.log(
    `${student.name} ${student.lastName} переведен на следующий курс`
  );
  for (let key in student.tabel) {
    counterRange++;
    sum += student.tabel[key];
    result = sum / counterRange;
    if (result > 7) {
      console.log(`${student.name} ${student.lastName} назначена стипендия`);
    }
  }
}
console.log(student);
