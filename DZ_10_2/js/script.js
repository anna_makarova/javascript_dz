"use strict";

const form = document.querySelector(".password-form");
const error = document.createElement("p");
const icon = document.querySelectorAll(".icon-password");

icon.forEach((icon) => {
  icon.addEventListener("click", function () {
    showIcon(icon);
  });
});
function showIcon(icon) {
  if (icon.classList.contains("fa-eye")) {
    icon.closest(".input-wrapper").firstElementChild.type = "text";
    icon.classList.remove("fa-eye");
    icon.classList.add("fa-eye-slash");
  } else {
    icon.closest(".input-wrapper").firstElementChild.type = "password";
    icon.classList.remove("fa-eye-slash");
    icon.classList.add("fa-eye");
  }
}
form.addEventListener("click", (event) => {
  if (event.target.tagName === "BUTTON") {
    const firstValue = document.getElementById("pass").value;
    const secondValue = document.getElementById("confirmpass").value;

    if (validationForPassword(firstValue, secondValue)) {
      alert(`You are welcome`);
      clearValue(event);
    } else {
      error.className = "red-style";
      error.textContent = "Нужно ввести одинаковые значения";
      event.target.before(error);
      clearValue(event);
    }
  }
  if (event.target.tagName === "INPUT") {
    error.remove();
  }
});
function clearValue(event) {
  document.getElementById("pass").value = "";
  document.getElementById("confirmpass").value = "";
  event.preventDefault();
}
function validationForPassword(firstValue, secondValue) {
  if (
    firstValue === secondValue &&
    firstValue &&
    secondValue &&
    secondValue.trim() !== "" &&
    firstValue.trim() !== ""
  ) {
    return firstValue, secondValue;
  }
}
