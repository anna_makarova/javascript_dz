"use strict";

let userName;
let userAge;

do {
  userName = prompt("Enter Your name: ");
} while (userName.trim() === "" || userName === null);
do {
  userAge = prompt("Enter Your age: ");
} while (userAge.trim() === "" || userAge === null || isNaN(+userAge));

if (userAge < 18) {
  alert(`You are not allowed to visit this website`);
} else if (userAge >= 18 && userAge <= 22) {
  const result = confirm(`Are you sure you want to continue?`);
  switch (result) {
    case true:
      alert(`Welcome, ${userName}`);
      break;

    case false:
      alert(`You are not allowed to visit this website`);
      break;
  }
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
}
