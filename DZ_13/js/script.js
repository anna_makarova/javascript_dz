"use strict";
const button = document.getElementById("changeColor"),
  linkTheme = document.getElementById("theme"),
  white = "./css/white_theme.css",
  dark = "./css/black_theme.css";

function сheckStorageTheme() {
  if (
    sessionStorage.getItem("theme") === white ||
    sessionStorage.getItem("theme") === undefined
  ) {
    return white;
  } else {
    return dark;
  }
}

window.onload = function () {
  linkTheme.setAttribute("href", сheckStorageTheme());
};
button.addEventListener("click", () => {
  console.log(linkTheme.getAttribute("href"));
  if (linkTheme.getAttribute("href") === white) {
    sessionStorage.setItem("theme", "./css/black_theme.css");
    linkTheme.setAttribute("href", sessionStorage.getItem("theme"));
  } else {
    sessionStorage.setItem("theme", "./css/white_theme.css");
    linkTheme.setAttribute("href", sessionStorage.getItem("theme"));
  }
});
