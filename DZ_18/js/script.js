"use strict";
const originObj = {
  name: "Anna",
  lastName: "Gudzenco",
  tabel: {
    mathematics: 12,
    english: 10,
    danIt: ["html100", "JavaScript100"],
  },
};
const copyObj = {};

for (let key in originObj) {
  if (typeof originObj[key] === "object" && originObj[key] !== null) {
    copyObj[key] = {};

    for (let key2 in originObj[key]) {
      copyObj[key][key2] = originObj[key][key2];
    }
  } else {
    copyObj[key] = originObj[key];
  }
}

originObj.lastName = "Makarova";

console.log(originObj);
console.log(copyObj);
